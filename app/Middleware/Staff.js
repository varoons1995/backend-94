'use strict'
/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

class Staff {
  /**
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Function} next
   */
  async handle ({ request ,auth ,response}, next) {
    // call next to advance the request
    console.log(request)
    try {
      let v = await auth.check();
      console.log(v);
    }catch{

    }
    await next()
  }
}

module.exports = Staff
